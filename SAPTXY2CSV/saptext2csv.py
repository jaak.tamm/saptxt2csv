# ======================
# imports
# ======================
import tkinter as tk
import os
from tkinter import ttk
from tkinter import Menu
from tkinter import messagebox as msg
from tkinter import filedialog as fd

import csv
import fundef
import mrf1fundef
import mrf3fundef


class OOP():
    def __init__(self):
        # Create instance
        self.win = tk.Tk()
        # Add a title
        self.win.title('SAPTXT2CSV')
        self.create_widgets()

    ##        # Change the main windows icon
    ##        self.win.iconbitmap('pyc.ico')

    def create_widgets(self):
        tabControl = ttk.Notebook(self.win)  # Create Tab Control

        tab1 = ttk.Frame(tabControl)  # Create a tab
        tabControl.add(tab1, text='FIFO')  # Add the tab
        tab2 = ttk.Frame(tabControl)  # Add a second tab
        tabControl.add(tab2, text='Routing')  # Make second tab visible
        tab3 = ttk.Frame(tabControl)  # Add a second tab
        tabControl.add(tab3, text='Inforecord')  # Make third tab visible

        tabControl.pack(expand=1, fill="both")  # Pack to make visible

        # LabelFrame using tab1 as the parent
        mighty = ttk.LabelFrame(tab1, text=' Mighty Python ')
        mighty.grid(column=0, row=0, padx=8, pady=4)

        # LabelFrame using tab1 as the parent
        mighty = ttk.LabelFrame(tab1, text='FIFO calculations')
        mighty.grid(column=0, row=0, padx=8, pady=4)

        # Modify adding a Labels using mighty as the parent
        mrf1_label = ttk.Label(mighty, text="MRF1 file locaton:")
        mrf1_label.grid(column=0, row=0, sticky='W')

        mrf3_label = ttk.Label(mighty, text="MRF3 file locaton:")
        mrf3_label.grid(column=0, row=1, sticky='W')

        fiforesultlocation_label = ttk.Label(mighty, text="Result files location:")
        fiforesultlocation_label.grid(column=0, row=2, sticky='W')

        # Adding a Textbox Entry widget
        self.mrf1 = tk.StringVar()
        mrf1_entered = ttk.Entry(mighty, width=50, textvariable=self.mrf1)
        mrf1_entered.grid(column=1, row=0, sticky='W')

        self.mrf3 = tk.StringVar()
        mrf3_entered = ttk.Entry(mighty, width=50, textvariable=self.mrf3)
        mrf3_entered.grid(column=1, row=1, sticky='W')

        self.fiforesult = tk.StringVar()
        fiforesult_entered = ttk.Entry(mighty, width=50, textvariable=self.fiforesult)
        fiforesult_entered.grid(column=1, row=2, sticky='W')

        # Modified Button Click Function
        def _browse_mrf1():
            input_file_name = fd.askopenfilename(defaultextension=".txt",
                                                 filetypes=[("Text Documents", "*.txt"), ("All Files", "*.*")])
            self.mrf1.set(input_file_name)

        def _browse_mrf3():
            input_file_name = fd.askopenfilename(defaultextension=".txt",
                                                 filetypes=[("Text Documents", "*.txt"), ("All Files", "*.*")])
            self.mrf3.set(input_file_name)

        def _browse_dir():
            result_file_dir = fd.askdirectory(initialdir=os.getcwd(), title='Please select a directory')
            self.fiforesult.set(result_file_dir)

        # Adding a File browseButtons
        mrf1button = ttk.Button(mighty, text="Browse MRF1 file", command=_browse_mrf1, width=18)
        mrf1button.grid(column=3, row=0, sticky='W')

        mrf3button = ttk.Button(mighty, text="Browse MRF3 file", command=_browse_mrf3, width=18)
        mrf3button.grid(column=3, row=1, sticky='W')

        resultbutton = ttk.Button(mighty, text="Select result folder", command=_browse_dir, width=18)
        resultbutton.grid(column=3, row=2, sticky='W')

        # Modified Button Click Function
        def click_me():
            # Input file locations and result directory
            mrf1file = self.mrf1.get()
            mrf3file = self.mrf3.get()
            result = self.fiforesult.get()

            # Result file paths
            mrf1csv = result + '/mrf1.csv'
            mrf3csv = result + '/mrf3.csv'
            purchdatacsv = result + '/purchdata.csv'

            # Reading text files
            fifo = mrf1fundef.mrf1list(mrf1file)
            purchases = mrf3fundef.mrf3list(mrf3file)

            # Converting list of purchases
            purchlist = mrf3fundef.matpurchlist(purchases)

            # Generates dictionary of stock quantities

            purchdata = mrf3fundef.fifodata(purchlist, fifo)

            # Creating result files
            fundef.makecsv(mrf1csv, fifo)
            fundef.makecsv(mrf3csv, purchases)
            fundef.makecsv(purchdatacsv, purchdata)

        # Adding a Button
        action = ttk.Button(mighty, text="Generate CSV", command=click_me, width=18)
        action.grid(column=1, row=3, sticky='W')


# ======================
# Start GUI
# ======================
oop = OOP()  # create an instance of the class
# use instance variable to call mainloop via win
oop.win.mainloop()