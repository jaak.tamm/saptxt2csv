import csv

def makecsv(fname, lname):
    with open(fname, 'w', newline='') as csvfile:
        tulemus = csv.writer(csvfile, delimiter=';',  quoting=csv.QUOTE_MINIMAL)
        for element in lname:
            tulemus.writerow(element)